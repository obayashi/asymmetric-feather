%% Feather star single feather simulation

clc, clear, close all;

amplitude = 40;
len_range = [130:10:240]./1000;
width_range = [75]./1000;
t_rise = [0.1,0.1,0.5,0.5,0.1,0.1,0.5,0.5];
t_fall = [0.5,1,0.5,1,0.5,1,0.5,1];
t_wait = [0.5,0.5,0.5,0.5,1,1,1,1];

thrust_mat = zeros(length(len_range)*length(width_range)*length(t_rise),1);

num_elem = 10;
count = 1;
output_fn = 'sim_data/220917_whold_horflaps_extra.txt';
fid = fopen(output_fn, 'w'); % delete whatever was in the file before

for i = 1:length(len_range)
    for j = 1:length(width_range)
        for k = 1:length(t_rise)
                
            % temp stuff
            tentacle.temp.compliance = 0;

            % geom data
            tentacle.geom.full_len = len_range(i);
            tentacle.geom.elem_len = tentacle.geom.full_len/num_elem;
            tentacle.geom.radi_root = width_range(j)/2;
            tentacle.geom.thick = 0.2e-3; %0.2mm
            tentacle.geom.num_elem = num_elem; 
            tentacle.geom.full_vol = tentacle.geom.full_len*tentacle.geom.radi_root*2* tentacle.geom.thick;
            tentacle.geom.elem_radi = zeros(1,num_elem) + tentacle.geom.radi_root;
            tentacle.geom.elem_vol = tentacle.geom.elem_len*tentacle.geom.thick*tentacle.geom.elem_radi*2;

            % material data
            tentacle.material.rho = 920; % 900 to 920
            tentacle.material.E =  120* 6894.76; %151685; % Pa 8
            tentacle.material
%                 tentacle.material.damp_elem = 1e-4; % need to determine later
%                 tentacle.material.eta = 0.0160; % impact test Hujare

            % signal data
            tentacle.signal.amplitude = deg2rad(amplitude); %rad
            tentacle.signal.p1 = 0;
            tentacle.signal.p2 = t_fall(k)/2;
            tentacle.signal.p3 = tentacle.signal.p2+t_wait(k);
            tentacle.signal.p4 = tentacle.signal.p3+t_rise(k);
            tentacle.signal.p5 = tentacle.signal.p4+t_wait(k);
            tentacle.signal.p6 = tentacle.signal.p5+(t_fall(k)/2);
            tentacle.signal.period = tentacle.signal.p6;

            % fluid data
            tentacle.fluid.rho = 997; % kg/m3
            tentacle.fluid.gravity = 9.80665; %m/s2
            tentacle.fluid.mu = 8.9e-4; % dynamic viscosity of water at 25 degC. Pa s

            % read aero coeff data
            rect_load = load('aero_coeff/rect_aero.mat').num;
            tentacle.aero.rect_CD = [rect_load(:,1) rect_load(:,2)]; % Re CD
            tentacle.aero.rect_CL = [rect_load(:,1) rect_load(:,3)]; % Re CL
            invv_load = load('aero_coeff/invv_aero.mat').num;
            tentacle.aero.invv_CD = [invv_load(:,1) invv_load(:,2)]; % Re CD
            tentacle.aero.invv_CL = [invv_load(:,1) invv_load(:,3)]; % Re CL
            
            % Calculate rotational k and d from textbook (Wittbrodt p.
            % 84) k_rot = E*J/len    d_rot = eta*J/len
            Iarea_rect = (1/12).*(tentacle.geom.elem_radi.*2.*(tentacle.geom.thick.^3));
            tentacle.material.kr_elem = (tentacle.material.E.*Iarea_rect)./(tentacle.geom.elem_len);
            tentacle.material.dr_elem = zeros(1,num_elem);

            % Calculate gravity and buoyancy force for each half element
            % Vector is in direction of gravity
            vol_half_elem = tentacle.geom.elem_vol./2;
            equiv_g_const = (1-(tentacle.fluid.rho./tentacle.material.rho)).*tentacle.fluid.gravity;
            force_grav_buoy = tentacle.material.rho.*equiv_g_const.*vol_half_elem; 
            tentacle.fluid.force_grav_buoy = zeros(tentacle.geom.num_elem,3);
%                 tentacle.fluid.force_grav_buoy(:,2) = force_grav_buoy*-1; 


            % Call simscape
            tic
            % cycles to run
            num_cycles = 10;
            sim_time = tentacle.signal.period * num_cycles;
            if tentacle.geom.num_elem == 10
                out = sim('tentacle_lumpedPara_10_wait_rev.slx',sim_time);
            elseif tentacle.geom.num_elem == 5
                out = sim('tentacle_lumpedPara_5.slx',sim_time);
            elseif tentacle.geom.num_elem == 20
                out = sim('tentacle_lumpedPara_20.slx',sim_time);
            end
            toc

            data = out.revjtF{1}.Values.Data(:,2); % y axis
            time = out.revjtF{1}.Values.Time;
            [val idx] = min(abs(time-tentacle.signal.period)); % idx at 1 cycle
            F_reac = data;
%                 F_gb = sum(force_grav_buoy)*2; % sum of force times 2 because the force vector is half elements
            F_gb = 0;
            F_thrust = (F_reac.*-1)-(-1*F_gb);
            F_thrust = F_thrust.*10000; % make thrust big so theres no cutoff when outputting
            plot(time,F_thrust)

%             blocks = [3*period/4:period:sim_time];
%             pos_vec = [];
%             neg_vec = [];
%             for z = 1:length(blocks)-1
%                 [temp first_idx] = min(abs(time-blocks(z)));
%                 [temp second_idx] = min(abs(time-blocks(z+1)));
%                 pos_vec(end+1) = max(F_thrust(first_idx:second_idx));
%                 neg_vec(end+1) = min(F_thrust(first_idx:second_idx));
%             end
%                
%             pos_thrust = mean(pos_vec);
%             neg_thrust = mean(neg_vec);

            pos_thrust = mean(F_thrust(F_thrust>0));
            neg_thrust = mean(F_thrust(F_thrust<0));
            avg_thrust = mean(F_thrust);

            output_vec = [amplitude len_range(i)*1000 width_range(j)*1000 t_rise(k) t_fall(k) t_wait(k) pos_thrust neg_thrust avg_thrust];
            
            % write to file
            fid = fopen(output_fn, 'a');
            fprintf(fid, '%f ',output_vec);
            fprintf(fid, '\n');
            
            str = strcat(num2str(count),' of ',num2str(length(thrust_mat(:,1))), ' done.')
            count = count + 1;
        end
    end
end
fclose(fid);
