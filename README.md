# Asymmetric feather

The files in this repository accompany the paper Control and Morphology Optimization of Passive Asymmetric Structures for Robotic Swimming. 

The code was written using MATLAB 2021a. 

This README highlights key codes/data used for this work. More details are available upon request. Please contact nana.obayashi@epfl.ch.



## main_rect_whold_kamran.m

Script to run Simscape simulation and calculate thrust data. 

## tentacle_lumpedPara_10_wait_rev.slx

Simscape simulation of a soft feather using the lumped parameter method.

## aero_coeff folder

Contains drag coefficient for a rectangular cross section obtained from Ansys Fluent.
